package com.example.piotr.kotlin_reddit_client.extensions

import org.mockito.Mockito

/**
 * @author piotr on 20.04.17.
 */

inline fun <reified T : Any> mock(): T = Mockito.mock(T::class.java)
