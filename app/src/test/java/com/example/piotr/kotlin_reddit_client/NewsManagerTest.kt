package com.example.piotr.kotlin_reddit_client

import com.example.piotr.kotlin_reddit_client.api.*
import com.example.piotr.kotlin_reddit_client.extensions.mock
import com.example.piotr.kotlin_reddit_client.model.RedditNews
import com.example.piotr.kotlin_reddit_client.news.NewsManager
import io.reactivex.observers.TestObserver
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Test
import org.mockito.Matchers.anyString
import org.mockito.Mockito.`when`
import retrofit2.Call
import retrofit2.Response
import java.util.*

/**
 * @author piotr on 20.04.17.
 */

class NewsManagerTest {

    var testObserver = TestObserver<RedditNews>()
    var apiMock = mock<NewsAPI>()
    var callMock = mock<Call<RedditNewsResponse>>()

    @Before
    fun setup() {
        testObserver = TestObserver.create<RedditNews>()
        apiMock = mock<NewsAPI>()
        callMock = mock<Call<RedditNewsResponse>>()
        `when`(apiMock.getNews(anyString(), anyString())).thenReturn(callMock)
    }

    @Test
    fun testSuccess_basic() {
        // prepare
        val redditNewsResponse = RedditNewsResponse(RedditDataResponse(listOf(), null, null))
        val response = Response.success(redditNewsResponse)

        `when`(callMock.execute()).thenReturn(response)

        // call
        val newsManager = NewsManager(apiMock)
        newsManager.getNews("").subscribe(testObserver)
        testObserver.awaitTerminalEvent()

        // assert
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()
    }

    @Test
    fun testSuccess_checkOneNews() {
        // prepare
        val newsData = RedditNewsDataResponse(
                "author",
                "title",
                10,
                Date().time,
                "thumbnail",
                "url"
        )
        val newsResponse = RedditChildrenResponse(newsData)
        val redditNewsResponse = RedditNewsResponse(RedditDataResponse(listOf(newsResponse), null, null))
        val response = Response.success(redditNewsResponse)

        `when`(callMock.execute()).thenReturn(response)

        // call
        val newsManager = NewsManager(apiMock)
        newsManager.getNews("").subscribe(testObserver)

        testObserver.awaitTerminalEvent()

        // assert
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()

        assert((testObserver.events[0] as List<RedditNews>)[0].news[0].author == newsData.author)
        assert((testObserver.events[0] as List<RedditNews>)[0].news[0].title == newsData.title)
    }

    @Test
    fun testError() {
        // prepare
        val responseError = Response.error<RedditNewsResponse>(500,
                ResponseBody.create(MediaType.parse("application/json"), ""))

        `when`(callMock.execute()).thenReturn(responseError)

        // call
        val newsManager = NewsManager(apiMock)
        newsManager.getNews("").subscribe(testObserver)

        testObserver.awaitTerminalEvent()

        // assert
        testObserver.assertError(Throwable::class.java)
    }
}