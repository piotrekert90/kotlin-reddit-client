package com.example.piotr.kotlin_reddit_client.news

import com.example.piotr.kotlin_reddit_client.api.NewsAPI
import com.example.piotr.kotlin_reddit_client.model.RedditNews
import com.example.piotr.kotlin_reddit_client.model.RedditNewsItem
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author piotr on 14.04.17.
 */
@Singleton
class NewsManager @Inject constructor(private val api: NewsAPI) {

    /**
     *
     * Returns Reddit News paginated by the given limit.
     *
     * @param after indicates the next page to navigate.
     * @param limit the number of news to request.
     */
    fun getNews(after: String, limit: String = "10"): Observable<RedditNews> {
        return Observable.create {
            val callResponse = api.getNews(after, limit)
            val response = callResponse.execute()

            if (response.isSuccessful) {
                val dataResponse = response.body().data
                val news = dataResponse.children.map {
                    val item = it.data
                    RedditNewsItem(item.author, item.title, item.num_comments,
                            item.created, item.thumbnail, item.url)
                }
                val redditNews = RedditNews(
                        dataResponse.after ?: "",
                        dataResponse.before ?: "",
                        news)

                it.onNext(redditNews)
                it.onComplete()
            } else {
                it.onError(Throwable(response.message()))
            }
        }
    }
}