package com.example.piotr.kotlin_reddit_client.news.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.example.piotr.kotlin_reddit_client.R
import com.example.piotr.kotlin_reddit_client.adapter.ViewType
import com.example.piotr.kotlin_reddit_client.adapter.ViewTypeDelegateAdapter
import com.example.piotr.kotlin_reddit_client.utils.inflate

/**
 * @author piotr on 12.04.17.
 */
class LoadingDelegateAdapter : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup) = TurnsViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
    }

    class TurnsViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
            parent.inflate(R.layout.news_item_loading))
}