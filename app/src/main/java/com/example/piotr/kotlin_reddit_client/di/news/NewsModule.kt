package com.example.piotr.kotlin_reddit_client.di.news

import com.example.piotr.kotlin_reddit_client.api.NewsAPI
import com.example.piotr.kotlin_reddit_client.api.NewsRestAPI
import com.example.piotr.kotlin_reddit_client.api.RedditApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * @author piotr on 22.04.17.
 */
@Module
class NewsModule {

    @Provides
    @Singleton
    fun provideNewsAPI(redditApi: RedditApi): NewsAPI {
        return NewsRestAPI(redditApi)
    }

    @Provides
    @Singleton
    fun provideRedditApi(retrofit: Retrofit): RedditApi {
        return retrofit.create(RedditApi::class.java)
    }
}
