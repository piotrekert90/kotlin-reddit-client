package com.example.piotr.kotlin_reddit_client.api

import retrofit2.Call
import javax.inject.Inject

/**
 * @author piotr on 17.04.17.
 */
class NewsRestAPI @Inject constructor(private val redditApi: RedditApi) : NewsAPI {

    override fun getNews(after: String, limit: String): Call<RedditNewsResponse> {
        return redditApi.getTop(after, limit)
    }
}