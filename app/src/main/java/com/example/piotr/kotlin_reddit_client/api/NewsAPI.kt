package com.example.piotr.kotlin_reddit_client.api

import retrofit2.Call

/**
 * @author piotr on 20.04.17.
 */
interface NewsAPI {
    fun getNews(after: String, limit: String): Call<RedditNewsResponse>
}