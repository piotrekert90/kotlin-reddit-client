package com.example.piotr.kotlin_reddit_client.adapter

/**
 * @author piotr on 12.04.17.
 */

object AdapterConstants {
    const val NEWS = 1
    const val LOADING = 2
}
