package com.example.piotr.kotlin_reddit_client

import android.support.v4.app.Fragment
import io.reactivex.disposables.CompositeDisposable

/**
 * @author piotr on 15.04.17.
 */

open class RxBaseFragment : Fragment() {

    protected var compositeDisposable = CompositeDisposable()

    override fun onResume() {
        super.onResume()
        compositeDisposable = CompositeDisposable()
    }

    override fun onPause() {
        super.onPause()
        if (compositeDisposable.isDisposed)
            compositeDisposable.clear()
    }
}
