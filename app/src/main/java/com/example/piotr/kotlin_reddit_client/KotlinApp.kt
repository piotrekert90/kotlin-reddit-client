package com.example.piotr.kotlin_reddit_client

import android.app.Application
import com.example.piotr.kotlin_reddit_client.di.AppModule
import com.example.piotr.kotlin_reddit_client.di.news.DaggerNewsComponent
import com.example.piotr.kotlin_reddit_client.di.news.NewsComponent

/**
 * @author piotr on 22.04.17.
 */
class KotlinApp : Application() {

    companion object {
        lateinit var newsComponent: NewsComponent
    }

    override fun onCreate() {
        super.onCreate()
        newsComponent = DaggerNewsComponent.builder()
                .appModule(AppModule(this))
                //.newsModule(NewsModule()) Module with empty constructor is implicitly created by dagger.
                .build()
    }
}