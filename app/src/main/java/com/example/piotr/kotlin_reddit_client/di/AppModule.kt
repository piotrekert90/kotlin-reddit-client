package com.example.piotr.kotlin_reddit_client.di

import android.content.Context
import com.example.piotr.kotlin_reddit_client.KotlinApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author piotr on 22.04.17.
 */
@Module
class AppModule(val app: KotlinApp) {

    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    @Singleton
    fun provideApplication(): KotlinApp = app
}
