package com.example.piotr.kotlin_reddit_client.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

/**
 * @author piotr on 12.04.17.
 */
interface ViewTypeDelegateAdapter {

    fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder

    fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType)
}