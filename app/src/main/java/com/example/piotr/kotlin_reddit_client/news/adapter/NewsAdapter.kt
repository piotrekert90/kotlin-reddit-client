package com.example.piotr.kotlin_reddit_client.news.adapter

import android.support.v4.util.SparseArrayCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.example.piotr.kotlin_reddit_client.adapter.AdapterConstants
import com.example.piotr.kotlin_reddit_client.adapter.ViewType
import com.example.piotr.kotlin_reddit_client.adapter.ViewTypeDelegateAdapter
import com.example.piotr.kotlin_reddit_client.model.RedditNewsItem

/**
 * @author piotr on 12.04.17.
 */

class NewsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: ArrayList<ViewType>
    private var delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter>()
    private val loadingItem = object : ViewType {
        override fun getViewType() = AdapterConstants.LOADING
    }

    init {
        delegateAdapters.run {
            put(AdapterConstants.LOADING, LoadingDelegateAdapter())
            put(AdapterConstants.NEWS, NewsDelegateAdapter())
        }
        items = ArrayList()
        items.add(loadingItem)
    }

    override fun getItemCount(): Int = items.size

    fun getItems(): ArrayList<ViewType> = items

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegateAdapters.get(viewType).onCreateViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegateAdapters.get(getItemViewType(position)).onBindViewHolder(holder, this.items[position])
    }

    override fun getItemViewType(position: Int) = this.items[position].getViewType()

    fun addNews(news: List<RedditNewsItem>) = with(items) {
        val initPosition: Int = size - 1

        //removes loading item and notify
        removeAt(initPosition)
        notifyItemRemoved(initPosition)

        // inserts news and the loading item at the end of the list
        addAll(news)
        add(loadingItem)
        notifyItemRangeChanged(initPosition, size + 1 /* plus loading item */)
    }


    fun clearAndAddNews(news: List<RedditNewsItem>) = with(items) {
        //clear array and notify news_list
        clear()
        notifyItemRangeRemoved(0, getLastPosition())

        //add news and notify news_list
        addAll(news)
        add(loadingItem)
        notifyItemRangeInserted(0, size)
    }

    fun getNews(): List<RedditNewsItem> = items
            .filter { it.getViewType() == AdapterConstants.NEWS }
            .map { it as RedditNewsItem }

    private fun getLastPosition() = if (items.lastIndex == -1) 0 else items.lastIndex

}
