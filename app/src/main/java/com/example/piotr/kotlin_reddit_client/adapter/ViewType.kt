package com.example.piotr.kotlin_reddit_client.adapter

/**
 * @author piotr on 12.04.17.
 */

interface ViewType {
    fun getViewType(): Int
}