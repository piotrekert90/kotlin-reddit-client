package com.example.piotr.kotlin_reddit_client.news.adapter

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.support.customtabs.CustomTabsIntent
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.example.piotr.kotlin_reddit_client.R
import com.example.piotr.kotlin_reddit_client.adapter.ViewType
import com.example.piotr.kotlin_reddit_client.adapter.ViewTypeDelegateAdapter
import com.example.piotr.kotlin_reddit_client.model.RedditNewsItem
import com.example.piotr.kotlin_reddit_client.utils.getFriendlyTime
import com.example.piotr.kotlin_reddit_client.utils.inflate
import com.example.piotr.kotlin_reddit_client.utils.loadImg
import kotlinx.android.synthetic.main.news_item.view.*


/**
 * @author piotr on 12.04.17.
 */

class NewsDelegateAdapter : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return TurnsViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        val context = holder.itemView.context
        holder as TurnsViewHolder
        holder.bind(item as RedditNewsItem)

        holder.itemView.setOnClickListener {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                val url = item.url
                val builder = CustomTabsIntent.Builder().run {
                    setToolbarColor(R.color.colorPrimary)
                    setSecondaryToolbarColor(R.color.colorPrimaryDark)

                    val description = "Share Link"

                    val bitmap = provideActionButtonBitmap(context)

                    val pendingIntent = provideActionButtonIntent(item, context)

                    setActionButton(bitmap!!, description, pendingIntent!!, true)
                }

                val customTabsIntent = builder.build()
                customTabsIntent.launchUrl(holder.itemView.context, Uri.parse(url))
            } else {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(item.url))
                context.startActivity(intent)
            }
        }
    }


    private fun provideActionButtonIntent(item: RedditNewsItem, context: Context?): PendingIntent? {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, item.url)

        val requestCode = 100

        val pendingIntent = PendingIntent.getActivity(context,
                requestCode,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT)
        return pendingIntent
    }

    private fun provideActionButtonBitmap(context: Context): Bitmap? = BitmapFactory.decodeResource(
            context.resources, R.drawable.abc_ic_menu_share_mtrl_alpha
    )

    class TurnsViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
            parent.inflate(R.layout.news_item)) {

        fun bind(item: RedditNewsItem) = with(itemView) {
            img_thumbnail.loadImg(item.thumbnail)
            description.text = item.title
            author.text = item.author
            comments.text = "${item.numComments} comments"
            time.text = item.created.getFriendlyTime()
        }
    }
}