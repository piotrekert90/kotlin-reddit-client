package com.example.piotr.kotlin_reddit_client.api

/**
 * @author piotr on 17.04.17.
 */
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface RedditApi {
    @GET("/top.json")
    fun getTop(@Query("after") after: String,
               @Query("limit") limit: String): Call<RedditNewsResponse>
}
