package com.example.piotr.kotlin_reddit_client.di.news

import com.example.piotr.kotlin_reddit_client.di.AppModule
import com.example.piotr.kotlin_reddit_client.di.NetworkModule
import com.example.piotr.kotlin_reddit_client.news.NewsFragment
import dagger.Component
import javax.inject.Singleton

/**
 * @author piotr on 22.04.17.
 */
@Singleton
@Component(modules = arrayOf(
        AppModule::class,
        NewsModule::class,
        NetworkModule::class)
)
interface NewsComponent {

    fun inject(newsFragment: NewsFragment)

}